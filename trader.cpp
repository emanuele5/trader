#include <iostream>
#include <thread>
#include <cpprest/http_client.h>
#include <cpprest/json.h>
#include <openssl/sha.h>
#include <openssl/hmac.h>

using web::http::client::http_client;
using web::http::http_request;
using web::http::http_response;
using web::http::methods;
using web::json::value;

static constexpr void b64encode(const uint8_t* src,char* dst)
{
	constexpr char table[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	for(unsigned i=0;i<63;i+=3)
	{
		uint32_t word=(src[i]<<16)+(src[i+1]<<8)+src[i+2];
		*dst++=table[(word>>18)&63];
		*dst++=table[(word>>12)&63];
		*dst++=table[(word>>6)&63];
		*dst++=table[word&63];
	}
	*dst++=table[src[63]>>2];
	*dst++=table[(src[63]<<4)&63];
	*dst++='=';
	*dst++='=';
}

static std::string GenerateNonce(void)
{
	return std::to_string(time(nullptr));
}

static double GetClosingPrice(const value& JsonObject)
{
	return stod(JsonObject.at(4).as_string());
}

class Trader
{
	double BTCAccount{};
	double EURAccount{};
	bool Oversold{};
	bool Overbought{};
	http_client Connection{"https://api.kraken.com"};
	value MarketData;

	static constexpr const uint8_t PrivKey[64]=
	{
	};
	static constexpr const char ApiKey[]="";

	static const char* GenerateSignature(std::string_view Uri,std::string_view Payload,std::string_view Nonce)
	{
		// sha 256 of nonce+payload
		std::string s1(Nonce);
		s1.append(Payload);
		uint8_t* hashresult=SHA256((const uint8_t*)s1.c_str(),s1.size(),nullptr);

		// generate signature
		std::string s2(Uri);
		s2.append((char*)hashresult,32);
		uint8_t* signature=HMAC(EVP_sha512(),PrivKey,64,(const uint8_t*)s2.c_str(),s2.size(),nullptr,nullptr);

		// signature to base64
		static char Result[89];
		b64encode(signature,Result);
		return Result;
	}

	value DoPublicRequest(const std::string& Uri,const std::string& Payload)
	{
		return Connection.request(methods::GET,Uri+'?'+Payload).get().extract_json().get();
	}

	value DoPrivateRequest(const std::string& Uri,const std::string& Payload,std::string_view Nonce)
	{
		http_request Request(methods::POST);
		Request.set_request_uri(Uri);
		Request.headers().add("API-Key",ApiKey);
		Request.headers().add("API-Sign",GenerateSignature(Uri,Payload,Nonce));
		Request.set_body(Payload,"application/x-www-form-urlencoded; charset=utf-8");
		return Connection.request(Request).get().extract_json().get();
	}

	public:
	
	void Run(void)
	{
		UpdateBalance();
		while(1)
		{
			std::thread t(&Trader::Check,this);
			t.detach();
			std::this_thread::sleep_for(std::chrono::minutes(5));
		}
	}

	void UpdateBalance(void)
	{	
		std::string Nonce=GenerateNonce();
		value Balance=DoPrivateRequest("/0/private/Balance","nonce="+Nonce,Nonce);
		BTCAccount=std::stod(Balance.at("result").at("XXBT").as_string());
		EURAccount=std::stod(Balance.at("result").at("ZEUR").as_string());
		std::cout<<"Current balance:\nEUR = "<<EURAccount<<"\nBTC = "<<BTCAccount<<'\n';
	}

	void UpdateMarketData(void)
	{
		MarketData=DoPublicRequest("/0/public/OHLC","pair=XBTEUR&interval=5&since="+std::to_string(time(nullptr)-60*5*15));
	}

	void SendOrder(const std::string& Type,double Quantity)
	{
		std::string Nonce=GenerateNonce();
		std::string Payload="nonce="+Nonce+"&ordertype=market&type="+Type+"&volume="+std::to_string(Quantity)+"&pair=XXBTZEUR";
		value Response=DoPrivateRequest("/0/private/AddOrder",Payload,Nonce);
		std::cout<<Response/*.at("result").at("descr").at("order")*/;
	}

	void Check(void)
	{
		UpdateMarketData();

		// calculate rsi
		double d{0},u{0};
		value& PriceArray=MarketData.at("result").at("XXBTZEUR");
		for(unsigned i=1;i<15;i++)
		{
			double diff=GetClosingPrice(PriceArray.at(i))-GetClosingPrice(PriceArray.at(i-1));
			//diff/=(15-i)/7.0;
			if(diff>0) u+=diff;
			else d-=diff;
		}
		double rsi=100.0-100.0/(1.0+u/d);
		std::cout<<"current RSI = "<<rsi<<'\n';

		// check for rsi signal
		if(rsi>70) Overbought=true;
		else if(rsi<30) Oversold=true;
		else if(Oversold && rsi>30)
		{
			// buy low
			double BTCQuantity=EURAccount/2/GetClosingPrice(PriceArray.at(14));
			SendOrder("buy",BTCQuantity);
			Oversold=false;
			UpdateBalance();
		}
		else if(Overbought && rsi<70)
		{
			// sell high
			SendOrder("sell",BTCAccount/2);
			Overbought=false;
			UpdateBalance();
		}
	}
};

int main(void)
{
	Trader trader;
	trader.Run();
}
